package applica;

import java.util.Locale;
import java.util.Scanner;

public class ExercicioTres 
{
	
	public static void main(String[] args) 
	{
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Escreva a temperatura em Celsius: ");
		double c = sc.nextDouble();
		
		double f = 1.8 * c + 32;
		
		System.out.printf("Temperatura em Fahrenheit: %.1f ", f);
		
		sc.close();
		
	}

}