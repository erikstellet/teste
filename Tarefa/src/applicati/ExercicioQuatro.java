package applicati;

import java.util.Locale;
import java.util.Scanner;

public class ExercicioQuatro {

	public static void main(String[] args)
	{
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		
		System.out.printf("Qual a potencia da lampada (em watts)? ");
		double pot = sc.nextDouble();
		
		System.out.printf("Qual a largura do comodo (em metros)? ");
		double larg = sc.nextDouble();
		
		System.out.printf("'Qual o comprimento do comodo (em metros)? ");
		
		double comp = sc.nextDouble();
		
		int result = (int) ((larg * comp) * 18) / (int)pot; 
		
		System.out.printf("'Numero de lampadas necessarias para iluminar esse comodo:" + result);
		
		sc.close();
	}

}