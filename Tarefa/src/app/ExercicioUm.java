package app;

import java.util.Locale;
import java.util.Scanner;

public class ExercicioUm
{
	
	public static void main(String[] args) 
	{
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Escreva a temperatura em Fahrenheit: ");
		double f = sc.nextDouble();
		
		double c = 5 * (f - 32 )/ 9;
		
		System.out.printf("Temperatura em Celsius: %.1f . ", c);
		
		sc.close();	
	}
}