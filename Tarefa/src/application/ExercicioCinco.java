package application;

import java.util.Locale;
import java.util.Scanner;

public class ExercicioCinco {

	public static void main(String[] args) 
	{
		double azul = 1.5;
		
		Locale.setDefault(Locale.US);
		Scanner sc = new Scanner(System.in);
		
		System.out.printf("Qual o comprimento da cozinha? ");
		double comp = sc.nextDouble(); 
				
		System.out.printf("Qual e a largura da cozinha? ");
		double larg = sc.nextDouble();
		
		System.out.printf("Qual e a altura da cozinha? ");
		double alt = sc.nextDouble();
		
		double result = ((alt*comp) * 2 + (alt*larg) * 2)/azul;
		
		System.out.printf("\n Resultado: " + result + " caixas");
		
		sc.close();
		
	}

}